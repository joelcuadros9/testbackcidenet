package com.cidenet.testback.service;

import com.cidenet.testback.model.Empleado;
import com.cidenet.testback.repository.EmpleadoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class EmpleadoService {

    @Autowired
    EmpleadoRepository empleadoRepository;

    public List<Empleado> obtenerEmpleados() {
        List<Empleado> empleados = new ArrayList<>();
        empleadoRepository.findAll().forEach(empleado -> empleados.add(empleado));
        return empleados;
    }

    public void guardarEmpleado(Empleado empleado){
        empleado.setCorreo(validarCorreo(empleado));
        empleadoRepository.save(empleado);
    }

    public void modificarEmpleado(Empleado empleado){
        empleado.setCorreo(validarCorreo(empleado));
        empleadoRepository.save(empleado);
    }

    public void eliminarEmpleado(int id){
        empleadoRepository.deleteById(id);
    }

    public String validarCorreo (Empleado empleado) {
        String codigo = "co";
        if(!empleado.getPaisEmpleo().equals("Colombia")) codigo = "us";
        String dominio = "@cidenet.com." + codigo;
        String datos = empleado.getPrimerNombre().toLowerCase() + "." + empleado.getPrimerApellido().toLowerCase();
        String correo = datos + dominio;
        List<Empleado> empleados = empleadoRepository.filtrarCorreos(datos);
        if(empleados.size() > 0){
            correo = datos + '.' + empleados.size() + dominio;
        }
        return correo;
    }

}
