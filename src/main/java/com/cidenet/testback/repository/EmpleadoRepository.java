package com.cidenet.testback.repository;

import com.cidenet.testback.model.Empleado;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmpleadoRepository extends CrudRepository<Empleado, Integer> {

    @Query("SELECT e FROM Empleado e WHERE e.primerNombre LIKE %:primerNombre% " +
            "OR e.otrosNombres LIKE %:otrosNombres% " +
            "OR e.primerApellido LIKE %:primerApellido% " +
            "OR e.segundoApellido LIKE %:segundoApellido% " +
            "OR e.tipoIdentificacion LIKE %:tipoIdentificacion% " +
            "OR e.numIdentificacion LIKE %:numIdentificacion% " +
            "OR e.paisEmpleo LIKE %:paisEmpleo% " +
            "OR e.correo LIKE %:correo%")
    List<Empleado> buscarEmpleadoLike(@Param("primerNombre") String primerNombre,
                                      @Param("otrosNombres") String otrosNombres,
                                      @Param("primerApellido") String primerApellido,
                                      @Param("segundoApellido") String segundoApellido,
                                      @Param("tipoIdentificacion") String tipoIdentificacion,
                                      @Param("numIdentificacion") String numIdentificacion,
                                      @Param("paisEmpleo") String paisEmpleo,
                                      @Param("correo") String correo);

    @Query("SELECT e FROM Empleado e WHERE e.correo LIKE %:correo% ")
    List<Empleado> filtrarCorreos(@Param("correo") String correo);
}
