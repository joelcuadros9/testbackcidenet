package com.cidenet.testback.controller;

import com.cidenet.testback.model.Empleado;
import com.cidenet.testback.service.EmpleadoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", methods= {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
@RestController
@RequestMapping("/empleado")
public class EmpleadoController {

    @Autowired
    EmpleadoService empleadoService;

    @GetMapping
    private List<Empleado> obtenerEmpleados()
    {
        return empleadoService.obtenerEmpleados();
    }

    @PostMapping
    private Empleado guardarEmpleado(@RequestBody Empleado empleado){
        empleadoService.guardarEmpleado(empleado);
        return empleado;
    }

    @PutMapping
    private Empleado modificarEmpleado(@RequestBody Empleado empleado){
        empleadoService.modificarEmpleado(empleado);
        return empleado;
    }

    @DeleteMapping("/{id}")
    private void eliminarEmpleado(@PathVariable("id") int id){
        empleadoService.eliminarEmpleado(id);
    }
}
